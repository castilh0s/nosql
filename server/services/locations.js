const service = require('feathers-memory');
const locations = require('../data/locations.json')

module.exports = service({
  store: locations
})